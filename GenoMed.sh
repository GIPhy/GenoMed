#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  GenoMed: Genome Medoid                                                                                    #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2022 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Bioinformatics and Biostatistics Hub                              research.pasteur.fr/en/team/hub-giphy  #
#   Dpt. Biologie Computationnelle            research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.0.220223ac                                                                                       #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Just give the execute permission to the script wgetENAHTS.sh with the following command line:             #
#                                                                                                            #
#   chmod +x GenoMed.sh                                                                                      #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ==============================                                                                             #
# = STATIC FILES AND CONSTANTS =                                                                             #
# ==============================                                                                             #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
  EXEC="sh -c";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echoxit() ============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = randfile =============================================================================================   #
#   creates and returns a random file name that does not exist from the specified basename $1                #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m GenoMed v$VERSION                $COPYRIGHT\033[0m";
  cat <<EOF

 USAGE:  GenoMed.sh  [OPTIONS]  <fasta1> <fasta2> <fasta3> [<fasta4> ...]

 OPTIONS:
  -r <int>    number of bootstrap replicates (default: 500)
  -t <int>    number of threads (default: 2)
  -h          prints this help and exits

EOF
}
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- flock ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  FLOCK_BIN=flock;
  [ ! $(command -v $FLOCK_BIN) ] && echoxit "no $FLOCK_BIN detected" ;
  FLOCK_STATIC_OPTIONS="";     
  FLOCK="$FLOCK_BIN $FLOCK_STATIC_OPTIONS";
#                                                                                                            #
# -- xargs ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
# - GNU xargs ---------------                                                                                #
  XARGS_BIN=xargs;
# - gxargs for mac OS X -----                                                                                #
  GXARGS_BIN=gxargs;
  XARGS_STATIC_OPTIONS="";     
  if   [ $(command -v $GXARGS_BIN) ]; then XARGS="$GXARGS_BIN $XARGS_STATIC_OPTIONS";
  elif [ $(command -v $XARGS_BIN)  ]; then XARGS="$XARGS_BIN $XARGS_STATIC_OPTIONS";
  else                                echoxit "neither $GXARGS_BIN nor $XARGS_BIN detected" ;
  fi
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  [ ! $(command -v $GAWK_BIN) ] && echoxit "no $GAWK_BIN detected" ;
  GAWK_STATIC_OPTIONS="";     
  GAWK="$GAWK_BIN $GAWK_STATIC_OPTIONS";
  TAWK="$GAWK -F\\t";
#                                                                                                            #
# -- mash -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  MASH_BIN=mash;
  [ ! $(command -v $MASH_BIN) ] && echoxit "no $MASH_BIN detected" ;
  MASH_STATIC_OPTIONS="";     
  MASH="$MASH_BIN $MASH_STATIC_OPTIONS";
  MSKETCH="$MASH sketch"
  MDIST="$MASH dist"
#                                                                                                            #
##############################################################################################################

##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export LC_ALL=C ;

BOOT=500;   # -r
NTHREADS=2; # -t

while getopts t:r:h option
do
  case $option in
  r)  BOOT=$OPTARG     ;;
  t)  NTHREADS=$OPTARG ;;
  h)  mandoc ;  exit 0 ;;
  :)  mandoc ;  exit 1 ;;
  \?) mandoc ;  exit 1 ;;
  esac
done

[[ $BOOT =~ ^[0-9]+$ ]] || echoxit "incorrect value: $BOOT (option -r)" ;
[ $BOOT -lt 1 ] && BOOT=1;

[ $NTHREADS -lt 1 ] && NTHREADS=1;
WAITIME=$($GAWK -v x=$NTHREADS 'BEGIN{print 1/sqrt(x)}');

shift "$(( $OPTIND - 1 ))"

## defining tmp files ########################################################################################
TMPDIR=${TMPDIR:-/tmp};
CMD=$(randfile $TMPDIR/genomed);
TMP=$(randfile $TMPDIR/genomed);
INPUT=$(randfile $TMPDIR/genomed);
NSATCG=$(randfile $TMPDIR/genomed);
OEPL=$(randfile $TMPDIR/genomed);

## defining traps ############################################################################################
finalizer() {
  rm -f $CMD $TMP $INPUT $NSATCG ;
}
trap 'finalizer;exit 1'  INT ERR TERM ;


##############################################################################################################
####                                                                                                      ####
#### CHECKING INPUT FILES                                                                                 ####
####                                                                                                      ####
##############################################################################################################

echo -n "reading input files ... " >&2 ;

## listing input files #######################################################################################
ls $@ | sort -b -i > $TMP 2>/dev/null ;
N=$(cat $TMP | wc -l);
[ $N -lt 3 ] && echoxit "insufficient no. input files: $N" ;
while read -r f
do
  [ ! -e $f ] && echoxit "file does not exist: $f" ;
  [ ! -s $f ] && echoxit "empty file: $f" ;
  [   -d $f ] && echoxit "directory: $f" ;
  [ ! -r $f ] && echoxit "no read permission: $f" ;
  echo $f ; 
done < $TMP > $INPUT
N=$(cat $INPUT | wc -l);
[ $N -lt 3 ] && echoxit "insufficient no. input files: $N" ;

## estimating ACGT content ###################################################################################
while read -r f
do
  echo "\"ns=\$(grep -c '^>' $f); at=\$(grep -v '^>' $f | tr -cd AaTt | wc -c); cg=\$(grep -v '^>' $f | tr -cd CcGg | wc -c); $FLOCK -x $NSATCG echo $f \$ns \$at \$cg >> $NSATCG ;\"" ;
done < $INPUT > $CMD ;

$XARGS -a $CMD -P $NTHREADS -I CMD bash -c CMD ;

sort $NSATCG > $TMP ; mv $TMP $NSATCG ;

echo "[ok]" >&2 ;
echo "no. files:     $N"  >&2 ;


##############################################################################################################
####                                                                                                      ####
#### SKETCHING INPUT FILES                                                                                ####
####                                                                                                      ####
##############################################################################################################

## estimating sketch size and k-mer length ###################################################################
#  S = avg genome length (at least 10000)
#  K = optimal length from the longest genome (at least 15)
S=$($GAWK '{s+=$3+$4}END{printf("%d\n", 100*int((s/NR)/100))}' $NSATCG);
echo "sketch size:   $S"  >&2 ;
[ $S -lt 10000 ] && S=10000;
K=$($GAWK '{l=$3+$4;g=(g>l)?g:l}END{printf("%d\n", int(log(g*(g-1))/log(4)))}' $NSATCG);
[ $(( $K % 2 )) -eq 0 ] && let K--;
[ $K -lt 15 ] && K=15;
echo "k-mer length:  $K"  >&2 ;

echo -n "sketching input files ... " >&2 ;

## defining traps ############################################################################################
rmmsh() {
  while read -r f; do rm -f ${f%.*}.msh; done < $INPUT ;
}
trap 'rmmsh;finalizer;exit 1'  INT ERR TERM ;

## sketching files ###########################################################################################
while read -r f
do
  echo "$MSKETCH -o ${f%.*} -s $S -k $K $f &> /dev/null" ;
done < $INPUT > $CMD ;

$XARGS -a $CMD -P $NTHREADS -I CMD bash -c CMD ;

echo "[ok]" >&2 ;


##############################################################################################################
####                                                                                                      ####
#### COMPUTING PAIRWISE DISTANCES                                                                         ####
####                                                                                                      ####
##############################################################################################################

echo -n "estimating pairwise distances ... " >&2 ;

tr '\n' ' ' < $INPUT > $OEPL ;
echo >> $OEPL ;
i=0;
while read -r fi
do
  i=$(( $i + 1 ));
  si=${fi%.*}.msh;
  j=0;
  while read -r fj
  do
    j=$(( $j + 1 ));
    if [ $i -le $j ]; then break ; fi
    sj=${fj%.*}.msh;
    echo "\"d=\$($MDIST $si $sj | $GAWK -v OFMT=%.8f '{print (1-exp(-\$3))}'); [ \${#d} -ne 0 ] && $FLOCK -x $OEPL echo $i $j \$d >> $OEPL ;\"" ;
  done < $INPUT
done < $INPUT > $CMD ;

$XARGS -a $CMD -P $NTHREADS -I CMD bash -c CMD ;

rmmsh ;

echo "[ok]" >&2 ;


##############################################################################################################
####                                                                                                      ####
#### COMPUTING MEDOID CRITERION TO MINIMIZE                                                               ####
####                                                                                                      ####
##############################################################################################################

$GAWK -v br=$BOOT 'function s(x){return x*x}
                   (ARGIND==1)        {ns[++x]=$2;
                                       lgt[x]=$3+$4; 
                                       a[x]=t[x]=$3/2;
                                       c[x]=g[x]=$4/2;
                                      }
                   (ARGIND==2&&FNR==1){while(++n<=NF){
                                         m=(m>(l=length(lbl[n]=$n)))?m:l; 
                                         d[n][n]=0; }
                                       --n; 
                                       m+=3;
                                       b=" ";x=0.5;while((x*=2)<m)b=b""b; 
                                       next; 
                                      }
                   (ARGIND==2)        {ai=a[$1]; ci=c[$1]; gi=g[$1]; ti=t[$1]; li=lgt[$1];
                                       aj=a[$2]; cj=c[$2]; gj=g[$2]; tj=t[$2]; lj=lgt[$2]
                                       b1=1-(s(ai+aj)+s(ci+cj)+s(gi+gj)+s(ti+tj))/s(li+lj);
                                       b2=1-(ai*aj+ci*cj+gi*gj+ti*tj)/(li*lj);
                                       dij=($3==0)?0:((x=1-$3/b2)>0)?-b1*log(x):-1;              # EI/F81 distance correction
                                       d[$1][$2]=(d[$2][$1]=dij);
                                       if(dij>=0){
                                         sum[$1]+=dij; norm[$1]++;
                                         sum[$2]+=dij; norm[$2]++; 
                                       } 
                                      }
                   END                {printf substr("#file"b,1,m);
                                       printf substr("nseq           ",1,7);
                                       printf substr("delta          ",1,10);
                                       print "p-value";
                                       srand(n);
                                       while(++r<=br){                                           # running bootstrap analysis
                                         i=0;while(++i<=n)card[i]=0;
                                         i=0;
                                         while(++i<=n){
                                           t[i]=ti=1+int(n*rand());                              # randomly picking ti in [1..n]
                                           card[ti]++;                                           # counting no. occurence(s) of ti
                                           c2[ti]++;                                             # c2[ti] = no. occurence(s) of ti
                                         }
                                         asort(t);                                               # sorting ids to reduce caclulations
                                         ibest=0;
                                         dbest=n;
                                         i=0;
                                         while(++i<=n){                                          # searching for ibest that minimizes delta
                                           ti=t[i];
                                           if(i>2&&ti==t[i-1])continue;                          # already processed thanks to the prev. sorting
                                           si=ni=j=0;
                                           while(++j<i) if((dij=d[ti][t[j]])>=0){si+=dij;++ni}
                                           while(++j<=n)if((dij=d[ti][t[j]])>=0){si+=dij;++ni}
                                           di=si/ni;
                                           if(di<dbest){
                                             ibest=ti;
                                             dbest=di; 
                                           }
                                         }
                                         if(ibest>0)c1[ibest]+=card[ibest];                      # c1[ibest] = no. occurence(s) of the medoid
                                       }
                                       i=0;
                                       while(++i<n){
                                         printf substr(lbl[i]b,1,m);
                                         printf substr(ns[i]"          ",1,7);
                                         printf("%.6f  %.3f\n",sum[i]/norm[i],c1[i]/c2[i]); 
                                       } 
                                      }'  $NSATCG  $OEPL  |  sort -k3g,3 ;


##############################################################################################################
####                                                                                                      ####
#### FINALIZING AND EXITING                                                                               ####
####                                                                                                      ####
##############################################################################################################

rm -f $CMD $TMP $INPUT $NSATCG ;

exit ;












## v1. standard solution #####################################################################################

# $GAWK 'function s(x){return x*x}
#        (ARGIND==1)        {ns[++x]=$2;
#                            sx=$3+$4;
#                            a[x]=t[x]=0.5*$3/sx;
#                            c[x]=g[x]=0.5*$4/sx; 
#                           }
#        (ARGIND==2&&FNR==1){while(++n<=NF){
#                              m=(m>(l=length(lbl[n]=$n)))?m:l; 
#                              d[n][n]=0; }
#                            --n; 
#                            m+=3;
#                            b=" ";x=0.5;while((x*=2)<m)b=b""b; 
#                            next; 
#                           }
#        (ARGIND==2)        {ai=a[$1]; ci=c[$1]; gi=g[$1]; ti=t[$1];
#                            aj=a[$2]; cj=c[$2]; gj=g[$2]; tj=t[$2];
#                            dij=($3==0)?0:((x=1-$3/(1-ai*aj-ci*cj-gi*gj-ti*tj))>0)?((s(ai+aj)+s(ci+cj)+s(gi+gj)+s(ti+tj))/4-1)*log(x):-1;
#                            if(dij>=0){
#                              sum[$1]+=dij; norm[$1]++;
#                              sum[$2]+=dij; norm[$2]++; } 
#                           }
#        END                {printf substr("#file"b,1,m);
#                            printf substr("nseq           ",1,7);
#                            print" criterion";
#                            while(++i<=n){
#                              printf substr(lbl[i]b,1,m);
#                              printf substr(ns[i]"          ",1,7);
#                              printf(" %.8f\n",sum[i]/norm[i]); }
#                           }'  $NSATCG  $OEPL  |  sort -k3g,3 ;


## v2. distance bootstrap ####################################################################################

# $GAWK -v br=$BOOT 'function s(x){return x*x}
#                    (ARGIND==1)        {ns[++x]=$2;
#                                        sx=$3+$4;
#                                        a[x]=t[x]=0.5*$3/sx;
#                                        c[x]=g[x]=0.5*$4/sx; 
#                                       }
#                    (ARGIND==2&&FNR==1){while(++n<=NF){
#                                          m=(m>(l=length(lbl[n]=$n)))?m:l; 
#                                          d[n][n]=0; }
#                                        --n; 
#                                        m+=3;
#                                        b=" ";x=0.5;while((x*=2)<m)b=b""b; 
#                                        next; 
#                                       }
#                    (ARGIND==2)        {ai=a[$1]; ci=c[$1]; gi=g[$1]; ti=t[$1];
#                                        aj=a[$2]; cj=c[$2]; gj=g[$2]; tj=t[$2];
#                                        dij=($3==0)?0:((x=1-$3/(1-ai*aj-ci*cj-gi*gj-ti*tj))>0)?((s(ai+aj)+s(ci+cj)+s(gi+gj)+s(ti+tj))/4-1)*log(x):-1;
#                                        d[$1][$2]=(d[$2][$1]=dij);
#                                        if(dij>=0){
#                                          sum[$1]+=dij; norm[$1]++;
#                                          sum[$2]+=dij; norm[$2]++; } 
#                                       }
#                    END                {printf substr("#file"b,1,m);
#                                        printf substr("nseq           ",1,7);
#                                        print "delta";
#                                        srand(n);
#                                        while(++i<n){
#                                          r=0;
#                                          while(++r<=br){
#                                            sr=nr=x=0;
#                                            while(++x<=n){
#                                              while((j=1+int(n*rand()))==i){}
#                                              if((dij=d[i][j])>=0){sr+=dij;++nr} }
#                                            db[r]=sr/nr; }
#                                          asort(db);
#                                          printf substr(lbl[i]b,1,m);
#                                          printf substr(ns[i]"          ",1,7);
#                                          printf("%.6f  [%.6f-%.6f]\n",sum[i]/norm[i],db[int(br/40)],db[int(39*br/40)]); } }'  $NSATCG  $OEPL  |  sort -k3g,3 ;
